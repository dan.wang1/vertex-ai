# Credit Card Fraud Detection with Vertex AI

This project involves training a credit card fraud detection model and deploying it to Google Cloud's Vertex AI platform.

## Prerequisites

- Python 3.6 or later
- Google Cloud account with the Vertex AI API enabled
- Google Cloud Storage (GCS) bucket

## Installation

Install the necessary Python packages with:

```
python3 -m pip install "kfp<2.0.0" "google-cloud-aiplatform>=1.16.0" --upgrade --quiet
```

## Usage

1. Train the model (replace with actual training script name).
2. Deploy the model with `python3 deploy.py`.
3. Make inference requests (replace with actual inference script name).
